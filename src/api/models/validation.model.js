const mongoose = require('mongoose');
const APIError = require('../utils/APIError');
const httpStatus = require('http-status');

/**
 * CheckIn
 * @private
 */
// const SchemaTypes = mongoose.Schema.Types;
const validationInfo = new mongoose.Schema({
  type: {
    type: String,
    required: true,
    unique : true,
  },
  message: {
    type: String,
    required: true,
  },
  isupdated: {
    type: Boolean,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});


/**
 * @typedef CheckIn
 */
module.exports = mongoose.model('validationInfo', validationInfo);
