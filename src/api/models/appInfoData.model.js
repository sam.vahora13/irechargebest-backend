const mongoose = require('mongoose');
const APIError = require('../utils/APIError');
const httpStatus = require('http-status');

/**
 * CheckIn
 * @private
 */
// const SchemaTypes = mongoose.Schema.Types;
const appInfoData = new mongoose.Schema({
 os_type: {
    type: String,
    required: true,
  },
  app_version: {
    type: String,
    required: true,
  },
  b2b_id: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});


/**
 * @typedef CheckIn
 */
module.exports = mongoose.model('appInfoData', appInfoData);
