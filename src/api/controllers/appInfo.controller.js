const httpStatus = require('http-status');
const { omit } = require('lodash');
const AppInfoData = require('../models/appInfoData.model');



/**
 * Create new AppInfoData
 * @public
 */
exports.appInfoData = async (req, res, next) => {
    try {
      const {os_type, app_version, b2b_id} = req.body;
      let appInfoData = {
        os_type,
        app_version,
        b2b_id
      }
      if(os_type === "android") {
        await AppInfoData.updateOne({ os_type :"android" }, appInfoData, {upsert: true}).exec();
      } else {
        await AppInfoData.updateOne({ os_type :"ios" }, appInfoData, {upsert: true}).exec();
      }
      res.status(httpStatus.CREATED);
      res.json({
          status: true,
          is_update: true,
          message: "App Info successfully added"         
      });
    } catch (error) {
      console.log("error >>>", error);
        res.status(httpStatus["400_NAME"]);
        return res.json({
            status: false,
            is_update: false,
            message: "App Info failed add"
        });
    }
  };


  /**
 * Create new Validation
 * @public
 */
exports.appInfoUpdate = async (req, res, next) => {
    try {
      const {type, message} = req.body;
      let validationReqData = {
          type: type,
          message: message
      }
      const validationData = new Validation(validationReqData);
      const savedvalidationData= await validationData.save();
      let validationUpdatedData = {
        type: "isupadted",
        isupdated: true
      }
      await Validation.updateOne({ type: "isupadted" }, validationUpdatedData, {upsert: true}).exec();
      res.status(httpStatus.CREATED);
      res.json({
        status: true,
        message: "validation successfully added",
        data: savedvalidationData
        });
    } catch (error) {
      console.log("error >>>", error);
    res.status(httpStatus["400_NAME"]);
    return res.json({
        status: false,
        message: "validation failed add"
    });
    }
  };