const httpStatus = require('http-status');
const { omit } = require('lodash');
const Validation = require('../models/validation.model');



/**
 * Create new Validation
 * @public
 */
exports.create = async (req, res, next) => {
    try {
      const {type, message} = req.body;
      let validationReqData = {
          type: type,
          message: message
      }
      const validationData = new Validation(validationReqData);
      const savedvalidationData= await validationData.save();
      let validationUpdatedData = {
        type: "isupadted",
        isupdated: true
      }
      await Validation.updateOne({ type: "isupadted" }, validationUpdatedData, {upsert: true}).exec();
      res.status(httpStatus.CREATED);
      res.json({
        status: true,
        message: "validation successfully added",
        data: savedvalidationData
        });
    } catch (error) {
      console.log("error >>>", error);
      if(error.code === 11000) {
        res.status(httpStatus["400_CLASS"]);
        return res.json({
            status: false,
            message: "validation key is duplicate."
        });

      }
    res.status(httpStatus["400_NAME"]);
    return res.json({
        status: false,
        message: "validation failed add"
    });
    }
  };

  /**
 * Edit Validation
 * @public
 */
exports.editValidation = async (req, res, next) => {
    try {
      const {id, type, message} = req.body;
      let validationReqUpdatedData = {
        type: type,
        message: message
    }
      const updatedvalidationDoc = await Validation.updateOne({ _id: id }, validationReqUpdatedData).exec();
      let validationUpdatedData = {
        type: "isupadted",
        isupdated: true
      }
      await Validation.updateOne({ type: "isupadted" }, validationUpdatedData, {upsert: true}).exec();
      if (updatedvalidationDoc.nModified >= 1) {
        res.status(httpStatus.ACCEPTED);
        return res.json({
          status: true,
          message: "validation successfully updated",
          });
      }
      return res.status(400).json({ status: false, message: 'validation failed updated.' });
    } catch (error) {
    res.status(httpStatus["400_NAME"]);
    return res.json({
        status: false,
        message: "validation failed updated"
    });
    }
  };


    /**
 * Get All Validation
 * @public
 */
exports.getAllValidation = async (req, res, next) => {
    try {
      const allValidationDoc = await Validation.find().sort({ createdAt: -1 }).exec();
      let validationUpdatedData = {
        type: "isupadted",
        isupdated: false
      }
      await Validation.updateOne({ type: "isupadted" }, validationUpdatedData, {upsert: true}).exec();
        res.status(httpStatus.ACCEPTED);
        return res.json({
          status: true,
          data: allValidationDoc
          });
    } catch (error) {
    res.status(httpStatus["400_NAME"]);
    return res.json({
        status: false,
        message: "There are no validation available."
    });
    }
  };

    /**
 * Delete Validation
 * @public
 */
exports.deleteValidation = async (req, res, next) => {
    try {
      const {id} = req.body;
      const doc = await Validation.deleteOne({ _id: id }).exec();
      let validationUpdatedData = {
        type: "isupadted",
        isupdated: true
      }
      await Validation.updateOne({ type: "isupadted" }, validationUpdatedData, {upsert: true}).exec();
      if (doc.deletedCount >= 1) {
        return res.status(200).json({ status: true, message: 'The validation successfully deleted.' });
      }
      return res.status(400).json({ status: false, message: 'The validation delete failed.' });
    } catch (error) {
    res.status(httpStatus["400_NAME"]);
    return res.json({
        status: false,
        message: "There are no validation available."
    });
    }
  };


  exports.isValidationUpdated = async (req, res, next) => {
    try {
      const allValidationDoc = await Validation.findOne({type: "isupadted"}).exec();
      if(!allValidationDoc.isupdated){
        res.status(httpStatus.ACCEPTED);
        return res.json({
          isUpdated: allValidationDoc.isupdated,
          message:"validation is updated"
          });
      } else {
        res.status(httpStatus.ACCEPTED);
        return res.json({
          isUpdated: allValidationDoc.isupdated,
          message:"validation is not updated"
          });
      }
    } catch (error) {
    res.status(httpStatus["400_NAME"]);
    return res.json({
        status: false,
        message: "There are no validation available."
    });
    }
  };